package com.elastic.demo;


import com.elastic.demo.elastic.model.Article;
import com.elastic.demo.elastic.model.Author;
import com.elastic.demo.elastic.model.Book;
import com.elastic.demo.elastic.repository.ArticleRepository;
import com.elastic.demo.elastic.repository.AuthorRepository;
import com.elastic.demo.elastic.repository.BookRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


@SpringBootApplication
public class DemoApplication {


    public static void main (String[] args) {

        final ConfigurableApplicationContext ctx = SpringApplication.run(DemoApplication.class, args);

        final BookRepository bookRepo = ctx.getBean(BookRepository.class);
        final AuthorRepository authorRepo = ctx.getBean(AuthorRepository.class);
        final ArticleRepository articleRepo = ctx.getBean(ArticleRepository.class);

        bookRepo.saveAll(createLibrary());
        authorRepo.saveAll(createAuthors());
        articleRepo.saveAll(createArticles());


    }


    public static List<Book> createLibrary () {

        List<Book> books = new ArrayList<>();
        for (int i = 0; i < 101; i++) {
            Book book = new Book();
            book.setPublishDate(LocalDateTime.now().toString());
            book.setText("Ana are mere vol " + i);
            book.setTitle("Cartea junglei " + i);
            book.setPrice(12.5 + i);
            book.setAuthors(Arrays.asList(createAuthors().get(i)));

            books.add(book);
        }
        return books;
    }


    public static List<Author> createAuthors () {

        List<Author> authors = new ArrayList<>();
        for (int i = 0; i < 101; i++) {
            Author author = new Author();
            author.setName("Ionut the " + i);
            author.setAge(i);
            author.setId(UUID.randomUUID());
            authors.add(author);
        }

        return authors;
    }


    public static List<Article> createArticles () {

        List<Article> articles = new ArrayList<>();

        for (int i = 0; i < 101; i++) {
            Article article = new Article();
            article.setTitle("Tom si jerry vol. " + i);
            article.setId("" + i);
            article.setPublishDate(LocalDateTime.now().toString());
            article.setText("Text " + i + " etc etc etc");
            article.setAuthors(Arrays.asList(createAuthors().get(i)));
            articles.add(article);
        }
        return articles;
    }


}
