package com.elastic.demo.elastic.repository;


import com.elastic.demo.elastic.model.Article;



public interface ArticleCustomRepository<T> {

    Iterable<Article> getAllArticlesWithElasticSearchTemplate();

    Iterable<Article> getArticlesWithAnAuthorWithName (String name);
}
