package com.elastic.demo.elastic.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.util.List;


@Document(indexName = "blog", type = "_doc")
@Data
public class Article {

    @Id
    private String id;

    @MultiField(mainField = @Field(type = FieldType.Text, fielddata = true),
        otherFields = {@InnerField(suffix = "verbatim", type = FieldType.Keyword)})
    private String title;

    @Field(type = FieldType.Nested, includeInParent = true)
    private List<Author> authors;

    @Field(type = FieldType.Text)
    private String text;

    @Field(type = FieldType.Text)
    private String publishDate;


}
