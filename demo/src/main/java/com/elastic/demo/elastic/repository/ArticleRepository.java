package com.elastic.demo.elastic.repository;


import com.elastic.demo.elastic.model.Article;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.stream.Stream;


@Repository
public interface ArticleRepository extends ElasticsearchRepository<Article, String>, ArticleCustomRepository {

    Stream<Article> streamByAuthorsContaining(String s);

    Iterable<Article> findAllByPublishDateEndingWith(String s);

}
