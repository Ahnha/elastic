package com.elastic.demo.elastic.repository;


import com.elastic.demo.elastic.model.Book;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;


public interface BookRepository extends ElasticsearchRepository<Book, String> {
}
