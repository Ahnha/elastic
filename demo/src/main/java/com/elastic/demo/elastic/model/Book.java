package com.elastic.demo.elastic.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.time.LocalDateTime;
import java.util.List;


@Document(indexName = "books", type = "_doc")
@Data
public class Book {

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String title;

    @Field(type = FieldType.Nested)
    private List<Author> authors;

    @Field(type = FieldType.Text)
    private String text;

    @Field(type = FieldType.Text)
    private String publishDate;

    @Field(type = FieldType.Double)
    private Double price;


}