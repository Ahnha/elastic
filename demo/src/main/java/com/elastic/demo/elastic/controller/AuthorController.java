package com.elastic.demo.elastic.controller;


import com.elastic.demo.elastic.model.Author;
import com.elastic.demo.elastic.repository.AuthorRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@AllArgsConstructor
public class AuthorController {


    private AuthorRepository authorRepository;


    @PostMapping(value = "/saveAuthors")
    public Object saveAuthors (@RequestBody List<Author> authors) {

        return authorRepository.saveAll(authors);
    }


    @GetMapping(value = "/get-authors")
    public Iterable<Author> getAuthors () {

        return authorRepository.findAllByQuery();
    }

    @GetMapping(value = "/get-authors-by-native")
    public Iterable<Author> getByNativeQueryAuthors (@RequestParam String s) {

        return authorRepository.findByAuthorsNameUsingCustomQuery(s);
    }
}
