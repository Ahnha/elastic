package com.elastic.demo.elastic.config;


import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.net.InetAddress;
import java.net.InetSocketAddress;


@Configuration
@EnableElasticsearchRepositories("com.elastic.demo.elastic")
public class Config {

    @Bean
    public Client client () throws Exception {

        org.elasticsearch.common.settings.Settings esSettings = org.elasticsearch.common.settings.Settings.builder()
            .put("cluster.name", "costel")
            .build();

        TransportClient tc = new PreBuiltTransportClient(esSettings);
        tc.addTransportAddress(new TransportAddress(new InetSocketAddress(InetAddress.getByName("127.0.0.1"), 9300)));

        return tc;
    }

}