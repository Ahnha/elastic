package com.elastic.demo.elastic.repository;


import com.elastic.demo.elastic.model.Author;
import org.springframework.data.elasticsearch.annotations.Query;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.UUID;


public interface AuthorRepository extends ElasticsearchRepository<Author, UUID> {


    @Query("{\"bool\": {\"must\": [{\"match\": {\"author.name\": \"?\"}}]}}")
    Iterable<Author> findByAuthorsNameUsingCustomQuery(String name);

    @Query("{\"match_all\": {}}")
    Iterable<Author> findAllByQuery();

}
