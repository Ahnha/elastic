package com.elastic.demo.elastic.controller;


import com.elastic.demo.elastic.model.Book;
import com.elastic.demo.elastic.repository.BookRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController(value = "books")
@AllArgsConstructor
public class BookController {

    private BookRepository bookRepository;

    @GetMapping(value = "/get-books")
    public Iterable<Book> getBooks () {

        return bookRepository.findAll();
    }

    @PostMapping(value = "/saveBooks")
    public Object saveBooks (@RequestBody List<Book> books){

        return bookRepository.saveAll(books);
    }
}
