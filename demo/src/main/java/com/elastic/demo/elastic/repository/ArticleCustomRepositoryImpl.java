package com.elastic.demo.elastic.repository;


import com.elastic.demo.elastic.model.Article;
import lombok.AllArgsConstructor;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.QueryBuilder;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Component;

import static org.elasticsearch.index.query.QueryBuilders.*;


@Component
@AllArgsConstructor
public class ArticleCustomRepositoryImpl implements ArticleCustomRepository {

    private ElasticsearchTemplate elasticsearchTemplate;


    @Override
    public Iterable<Article> getAllArticlesWithElasticSearchTemplate () {

        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(matchAllQuery()).withIndices("blog").build();
        return elasticsearchTemplate.queryForList(searchQuery, Article.class);
    }


    @Override
    public Iterable<Article> getArticlesWithAnAuthorWithName (String name) {

        QueryBuilder queryBuilder = nestedQuery("authors", boolQuery().must(termQuery("authors.name", name)),
            ScoreMode.None
        );

        SearchQuery searchQuery = new NativeSearchQueryBuilder().withQuery(queryBuilder).build();
        return elasticsearchTemplate.queryForList(searchQuery, Article.class);

    }
}
