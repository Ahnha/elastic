package com.elastic.demo.elastic.controller;


import com.elastic.demo.elastic.model.Article;
import com.elastic.demo.elastic.repository.ArticleRepository;
import com.elastic.demo.elastic.repository.AuthorRepository;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Stream;


@RestController
@AllArgsConstructor
public class ArticleController {

    private ArticleRepository articleRepository;

    private AuthorRepository authorRepository;



    @PostMapping(value = "/saveArticles")
    public Object saveArticles (@RequestBody List<Article> articles) {

        return articleRepository.saveAll(articles);
    }


    @DeleteMapping(value = "/delete-all")
    public void deleteAll () {

       // bookRepository.deleteAll();
        authorRepository.deleteAll();
        articleRepository.deleteAll();
    }


    @GetMapping(value = "/get-articles")
    public Iterable<Article> getArticles () {

        return articleRepository.getAllArticlesWithElasticSearchTemplate();
    }


    @GetMapping(value = "/get-articles-stream")
    public Stream<Article> getStreamArticles (@RequestParam String s) {
        return articleRepository.streamByAuthorsContaining(s);
    }

    @GetMapping(value = "/get-articles-publish")
    public Iterable<Article> getByPublishDateArticles (@RequestParam String s) {
        return articleRepository.findAllByPublishDateEndingWith(s);
    }

    @GetMapping(value = "/get-articles-by-authors-name")
    public Iterable<Article> getArticlesByAuthorName (@RequestParam String s) {
        return articleRepository.getArticlesWithAnAuthorWithName(s);
    }


}
